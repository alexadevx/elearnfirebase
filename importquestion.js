const firebase = require("firebase");
// Required for side-effects
require("firebase/firestore");
const fs = require('fs');

let rawdata = fs.readFileSync('android.json');
let menu = JSON.parse(rawdata);


// Initialize Cloud Firestore through Firebase
firebase.initializeApp({
    apiKey: "AIzaSyB2yEvLiZbhmmfxt4A6mNwIxRXJZZdm6WM",
    authDomain: "https://elearn-3b2a9.firebaseio.com",
    projectId: "elearn-3b2a9"
  });
  
  
var db = firebase.firestore();

menu.forEach(function(obj) {
    db.collection("iqanda").add({
      qquestions: obj.Questions,
      qanswer: obj.Answer,
        qdifficulty: obj.Difficulty,
        qtopicid:obj.Topic
    }).then(function(docRef) {
        console.log("Document written with ID: ", docRef.id);
    })
    .catch(function(error) {
        console.error("Error adding document: ", error);
    });
});