const firebase = require("firebase");
// Required for side-effects
require("firebase/firestore");

// Initialize Cloud Firestore through Firebase
firebase.initializeApp({
    apiKey: "AIzaSyB2yEvLiZbhmmfxt4A6mNwIxRXJZZdm6WM",
    authDomain: "https://elearn-3b2a9.firebaseio.com",
    projectId: "elearn-3b2a9"
  });
  
  
var db = firebase.firestore();

var menu =[  
    {  
       "tid":1,
       "tname":"Android",
       "ticon":"android.png",    
    },
    {  
        "tid":2,
        "tname":"Java",
        "ticon":"java.png",    
     },
     {  
        "tid":3,
        "tname":"Flutter",
        "ticon":"flutter.png",    
     },
 ]

menu.forEach(function(obj) {
    db.collection("itopics").add({
        tid: obj.tid,
        ticon: obj.ticon,
        tname: obj.tname
    }).then(function(docRef) {
        console.log("Document written with ID: ", docRef.id);
    })
    .catch(function(error) {
        console.error("Error adding document: ", error);
    });
});